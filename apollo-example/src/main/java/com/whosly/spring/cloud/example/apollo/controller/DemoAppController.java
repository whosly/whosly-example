package com.whosly.spring.cloud.example.apollo.controller;

import com.whosly.spring.cloud.example.apollo.config.data.AppConfigData;
import com.whosly.spring.cloud.example.apollo.service.IDemoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
@RequestMapping("/app")
@Slf4j
public class DemoAppController {

    @Autowired
    private AppConfigData appConfigData;

    @Autowired
    private IDemoService demoService;

    @GetMapping("/now")
    public String now() {
        LocalDate date = LocalDate.now();

        log.info("now1:{}.", date.toString());
        log.warn("now2:{}.", date.toString());
        log.error("now3:{}.", date.toString());
        log.error("appConfigData :{}.", appConfigData);

        demoService.get();

        return date.toString();
    }

    @GetMapping("/error")
    public String error() {
        log.error("appConfigData :{}.", appConfigData);

        demoService.get();

        LocalDate date = LocalDate.now();

        return date.toString();
    }

}

package com.whosly.spring.cloud.example.apollo.service;

public interface IDemoService {

    String get();

}

package com.whosly.spring.cloud.example.apollo.service.impl;

import com.whosly.spring.cloud.example.apollo.service.IDemoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DemoServiceImpl implements IDemoService {

    @Value("${aaaaaa}")
    private String aaaaaa;

    @Value("${bbb}")
    private String bbb;

    @Override
    public String get() {
        log.error("a :{}.", aaaaaa);
        log.error("bbb :{}.", bbb);

        return bbb;
    }

}

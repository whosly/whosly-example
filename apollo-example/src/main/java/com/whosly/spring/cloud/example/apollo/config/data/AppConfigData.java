package com.whosly.spring.cloud.example.apollo.config.data;

import com.ctrip.framework.apollo.model.ConfigChangeEvent;
import com.ctrip.framework.apollo.spring.annotation.ApolloConfigChangeListener;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Setter
@ToString
@Configuration
@ConfigurationProperties(prefix = "app")
public class AppConfigData implements IDynamicData {

    /**
     * a
     */
    @Getter
    private String a;

    /**
     *
     */
    @Getter
    private String b;

    /**
     *
     */
    @Getter
    private String c;


    /**
     * apollo配置监听器，默认监听的是application命名空间
     *
     * @param changeEvent
     */
    @ApolloConfigChangeListener("application")
    public void onChange(ConfigChangeEvent changeEvent) {
        refresh("app.", changeEvent.changedKeys(),
                this.getClass().getCanonicalName());
    }

}

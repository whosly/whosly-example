package com.whosly.spring.cloud.example.apollo.config;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Configuration;

/**
 * apollo 配置
 */
@Configuration
@EnableApolloConfig(value = {"application"})
@Slf4j
public class ApolloConfiguration implements InitializingBean {

    @Override
    public void afterPropertiesSet() throws Exception {
        log.info("Configuration load");
    }

}
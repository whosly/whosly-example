package com.whosly.spring.cloud.example.sentry.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
@RequestMapping("/app")
@Slf4j
public class DemoAppController {

    @GetMapping("/now")
    public String now() {
        LocalDate date = LocalDate.now();

        log.info("now1:{}.", date.toString());
        log.warn("now2:{}.", date.toString());
        log.error("now3:{}.", date.toString());

        return date.toString();
    }

    @GetMapping("/error")
    public String error() {
        LocalDate date = LocalDate.now();

        return date.toString();
    }

}

package com.whosly.spring.cloud.example.sentry;

import java.io.IOException;

import com.whosly.spring.cloud.components.starter.sentry.EnableSentry;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 */
@SpringBootApplication
@EnableSentry
public class SpringBootSampleApplication {

  public static void main(String[] args) throws IOException {
      ApplicationContext context = SpringApplication
              .run(SpringBootSampleApplication.class, args);
  }

}

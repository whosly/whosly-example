package com.whosly.spring.cloud.example.sentry;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 */
@SpringBootApplication
public class SentrySpringBootSampleApplication {

  public static void main(String[] args) throws IOException {
      ApplicationContext context = SpringApplication
              .run(SentrySpringBootSampleApplication.class, args);
  }

}
